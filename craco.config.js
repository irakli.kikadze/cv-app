module.exports = {
  style: {
    postcss: {
      plugins: [require("autoprefixer")],
    },
  },
  sass: {
    loaderOptions: {
      implementation: require("node-sass"),
    },
  },
};
