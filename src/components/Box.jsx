import React from "react";
import "./../assets/styles/Box.scss";

const Box = (props) => {
  const { title, content } = props;

  return (
    <>
      <h2 className="about-me" id="about-me">
        {title}
      </h2>
      <div className="box">
        <p className="content">{content}</p>
      </div>
    </>
  );
};

export default Box;
