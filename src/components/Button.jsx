import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Button = ({ icon, text }) => {
  return (
    <button>
      {icon && <FontAwesomeIcon icon={icon} data-testid="button-icon" />} {text}
    </button>
  );
};

export default Button;
