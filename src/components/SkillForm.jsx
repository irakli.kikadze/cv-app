import React from "react";
import { useFormik } from "formik";
import "./../assets/styles/SkillForm.scss";

const SkillForm = ({ onSubmit }) => {
  const formik = useFormik({
    initialValues: {
      skillName: "",
      skillRange: "",
    },
    validate: (values) => {
      const errors = {};
      if (!values.skillName) {
        errors.skillName = "Skill name is a required field";
      }
      if (!values.skillRange) {
        errors.skillRange = "Skill range is a required field";
      } else if (isNaN(values.skillRange)) {
        errors.skillRange = 'Skill range must be a "number" type';
      } else if (values.skillRange < 10) {
        errors.skillRange = "Skill range must be greater than or equal to 10";
      } else if (values.skillRange > 100) {
        errors.skillRange = "Skill range must be less than or equal to 100";
      }
      return errors;
    },
    onSubmit: (values) => {
      onSubmit(values);
      formik.resetForm();
    },
  });

  const handleValidation = (fieldName) => {
    if (formik.errors[fieldName]) {
      return "error";
    } else if (formik.values[fieldName]) {
      return "success";
    }
    return "";
  };

  return (
    <form
      data-testid="skill-form"
      onSubmit={formik.handleSubmit}
      className="form-container"
    >
      <div className="input-container">
        <label className="form-label" htmlFor="skillName">
          Skill Name:
        </label>
        <input
          type="text"
          name="skillName"
          onChange={formik.handleChange}
          value={formik.values.skillName}
          className={`${handleValidation("skillName")} form-input`}
          placeholder="Enter skill name"
        />
        {formik.errors.skillName && (
          <div className="error-message">{formik.errors.skillName}</div>
        )}
      </div>
      <div className="input-container">
        <label className="form-label" htmlFor="skillRange">
          Skill Range:
        </label>
        <input
          type="number"
          name="skillRange"
          onChange={formik.handleChange}
          value={formik.values.skillRange}
          className={`${handleValidation("skillRange")} form-input`}
          placeholder="Enter skill range"
        />
        {formik.errors.skillRange && (
          <div className="error-message">{formik.errors.skillRange}</div>
        )}
      </div>
      <button
        className="form-button"
        type="submit"
        disabled={Object.keys(formik.errors).length !== 0}
      >
        Add skill
      </button>
    </form>
  );
};

export default SkillForm;
