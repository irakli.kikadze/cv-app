import { useEffect, useState } from "react";
import { addSkill } from "./../store/skills-action";
import SkillForm from "./SkillForm";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import "./../assets/styles/Skills.scss";
import React from "react";

const Skills = () => {
  const dispatch = useDispatch();
  const skills = useSelector((state) => state.skills.skills);
  const [isVisible, setIsVisible] = useState(false);

  const handleSubmit = (values) => {
    dispatch(addSkill(values));
  };

  const toggleForm = () => {
    setIsVisible(!isVisible);
  };

  return (
    <>
      <div className="skills-header">
        <h2 className="skills" id="skills">
          Skills
        </h2>
        <div onClick={toggleForm} className="skills-button">
          <FontAwesomeIcon icon={faEdit} />
          Open Edit
        </div>
      </div>
      {isVisible && <SkillForm onSubmit={handleSubmit} />}
      {skills.map((skill, index) => (
        <div key={index} className="skill" style={{ width: `${skill.range}%` }}>
          {skill.name}
        </div>
      ))}
      <div className="skills-ranges">
        <div className="skills-range skills-range-1"></div>
        <div className="skills-range skills-range-2"></div>
        <div className="skills-range skills-range-3"></div>
        <div className="skills-range skills-range-4"></div>
      </div>
      <div className="range-levels">
        <div className="range-level range-level-1">Beginner</div>
        <div className="range-level range-level-2">Proficient</div>
        <div className="range-level range-level-3">Expert</div>
        <div className="range-level range-level-4">Master</div>
      </div>
    </>
  );
};

export default Skills;
