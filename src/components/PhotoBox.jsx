import React from "react";

const PhotoBox = ({ name, title, description, avatar, className }) => {
  return (
    <div className={className[0]}>
      <img className={className[1]} src={avatar} alt="User Avatar" />
      <div className={`username ${className[2]}`}>{name}</div>
      {title && (
        <p style={{ fontSize: "25px", marginBottom: "15px" }}>{title}</p>
      )}
      {description && <p style={{ marginBottom: "15px" }}>{description}</p>}
    </div>
  );
};

export default PhotoBox;
