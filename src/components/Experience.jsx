import React from "react";
import "./../assets/styles/Experience.scss";
const Experience = ({ data }) => {
  return (
    <>
      <h2 id="experience">Experience</h2>
      <div className="experience">
        {data.map((experience, index) => (
          <div className="experience-wrapper" key={index}>
            <div className="experience-col">
              <p className="experience-company">{experience.info.company}</p>
              <p className="experience-date"> {experience.date}</p>
            </div>
            <div className="experience-col2">
              <p className="experience-job">Job: {experience.info.job}</p>
              <p className="experience-description">
                Description: {experience.info.description}
              </p>
            </div>
            <hr />
          </div>
        ))}
      </div>
    </>
  );
};

export default Experience;
