import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSync } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import "./../assets/styles/TimeLine.scss";

const TimeLine = ({ data }) => {
  if (data === null) {
    return (
      <>
        <h2 className="education" id="education">
          Education
        </h2>
        {data === null && (
          <div className="errorMessage">
            Something went wrong; please review your server connection!
          </div>
        )}
      </>
    );
  }

  return (
    <>
      <h2 className="education" id="education">
        Education
      </h2>
      {data?.length === 0 && (
        <FontAwesomeIcon
          className="syncIcon"
          data-testid="sync-icon"
          color="rgba(38, 193, 126, 1)"
          size="2x"
          icon={faSync}
        />
      )}
      <div className="timeline">
        {data.map((event, index) => (
          <div className="timeline-wrapper" key={index}>
            <div className="timeline-date-wrapper">
              <p className="timeline-date">{event.date}</p>
              {index !== data.length - 1 && (
                <div className="timeline-line"></div>
              )}
            </div>
            <div key={index} className="timeline-event">
              <h3 className="timeline-title">{event.title}</h3>
              <p className="timeline-text">{event.text}</p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default TimeLine;
