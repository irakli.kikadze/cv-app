import React from "react";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faSkype,
  faFacebookF,
  faTwitter,
  faphone,
} from "@fortawesome/free-brands-svg-icons";
import "./../assets/styles/Address.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Address = ({ phone, email, twitter, facebook, skype }) => {
  const handleEmailClick = () => {
    window.location.href = `mailto:${email}`;
  };

  const handlePhoneClick = () => {
    window.location.href = `tel:${phone}`;
  };

  return (
    <>
      <h2 className="contacts" id="contacts">
        Contacts
      </h2>
      <div className="address">
        <div>
          <p className="address-phone">
            <FontAwesomeIcon
              size="2x"
              icon={faPhone}
              color="rgba(38, 193, 126, 1)"
            />
            <a
              className="phone-num"
              href={`tel:${phone}`}
              onClick={handlePhoneClick}
            >
              {phone}
            </a>
          </p>

          <p className="address-email">
            <FontAwesomeIcon
              size="2x"
              icon={faEnvelope}
              color="rgba(38, 193, 126, 1)"
            />
            <a
              className="phone-num"
              href={`mailto:${email}`}
              onClick={handleEmailClick}
            >
              {email}
            </a>
          </p>
          <div className="address-twitter">
            <FontAwesomeIcon
              size="2x"
              icon={faTwitter}
              color="rgba(38, 193, 126, 1)"
            />
            <div className="address-skype-inner">
              <p className="address-skype-h2">Twitter</p>
              <a
                className="address-skype-link"
                href={twitter}
                onClick={handlePhoneClick}
              >
                {twitter}
              </a>
            </div>
          </div>
          <div className="address-facebook">
            <FontAwesomeIcon
              size="2x"
              icon={faFacebookF}
              color="rgba(38, 193, 126, 1)"
            />
            <div className="address-skype-inner">
              <p className="address-skype-h2">Facebook</p>
              <a
                className="address-skype-link"
                href={facebook}
                onClick={handlePhoneClick}
              >
                {facebook}
              </a>
            </div>
          </div>
          <div className="address-skype">
            <FontAwesomeIcon
              size="2x"
              icon={faSkype}
              color="rgba(38, 193, 126, 1)"
            />
            <div className="address-skype-inner">
              <p className="address-skype-h2">Skype</p>
              <a
                className="address-skype-link"
                href={skype}
                onClick={handlePhoneClick}
              >
                {skype}
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Address;
