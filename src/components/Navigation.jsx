import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faGraduationCap,
  faPencil,
  faSuitcase,
  faComment,
  faLocationArrow,
  faDiamond,
} from "@fortawesome/free-solid-svg-icons";
import "./../assets/styles/Navigation.scss";

const Navigation = () => {
  const [active, setActive] = useState(0);

  const handleItemClick = (index, id) => {
    setActive(index);
    const element = document.getElementById(id);
    if (element) {
      element.scrollIntoView({ behavior: "smooth" });
    }
  };

  const navItems = [
    { text: "About me", icon: faUser, id: "about-me" },
    { text: "Education", icon: faGraduationCap, id: "education" },
    { text: "Experience", icon: faPencil, id: "experience" },
    { text: "Skills", icon: faDiamond, id: "skills" },
    { text: "Portfolio", icon: faSuitcase, id: "portfolio" },
    { text: "Contacts", icon: faLocationArrow, id: "contacts" },
    { text: "Feedbacks", icon: faComment, id: "feedbacks" },
  ];

  useEffect(() => {
    const handleScroll = () => {
      const windowHeight = window.innerHeight;
      const elements = navItems.map((item) => document.getElementById(item.id));

      elements.forEach((element, index) => {
        if (element) {
          const { top } = element.getBoundingClientRect();
          if (element.id === "feedbacks" && top < windowHeight * 0.4) {
            setActive(index);
          } else if (top < windowHeight * 0.1) {
            setActive(index);
          }
        }
      });
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [navItems]);

  return (
    <div>
      {navItems.map((item, index) => (
        <div
          key={index}
          onClick={() => handleItemClick(index, item.id)}
          className={`navigation-item ${active === index ? "active" : ""}`}
          style={item.id === "feedbacks" ? { marginBottom: 0 } : {}}
        >
          <span>
            {item.icon && (
              <FontAwesomeIcon
                icon={item.icon}
                className="navigation-item-icon"
              />
            )}
          </span>
          <span className="navigation-item-text"> {item.text}</span>
        </div>
      ))}
    </div>
  );
};

export default Navigation;
