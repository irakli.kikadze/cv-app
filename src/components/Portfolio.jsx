import React, { useState } from "react";
import ui from "./../assets/images/ui.png";
import code from "./../assets/images/code.png";
import "./../assets/styles/Portfolio.scss";

const Portfolio = () => {
  const [filter, setFilter] = useState("all");

  const portfolioItems = [
    {
      id: 1,
      category: "design",
      title: "Some text",
      description:
        "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
    },
    {
      id: 2,
      category: "development",
      title: "Some text",
      description:
        "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
    },
    {
      id: 3,
      category: "design",
      title: "Some text",
      description:
        "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
    },
    {
      id: 4,
      category: "development",
      title: "Some text",
      description:
        "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
    },
  ];

  const handleFilterChange = (filter) => {
    setFilter(filter);
  };

  const filteredItems =
    filter === "all"
      ? portfolioItems
      : portfolioItems.filter((item) => item.category === filter);

  return (
    <div>
      <h2 className="portfolio" id="portfolio">
        Portfolio
      </h2>
      <div className="portfolio-buttons">
        <div
          className={filter === "all" ? "active" : ""}
          onClick={() => handleFilterChange("all")}
        >
          All
        </div>
        <span>/</span>
        <div
          className={filter === "design" ? "active" : ""}
          onClick={() => handleFilterChange("design")}
        >
          Code
        </div>
        <span>/</span>
        <div
          className={filter === "development" ? "active" : ""}
          onClick={() => handleFilterChange("development")}
        >
          UI
        </div>
      </div>
      <div className="card-container">
        {filteredItems.map((item) => (
          <div className="card" key={item.id}>
            <img
              src={item.category === "design" ? ui : code}
              alt={item.title}
            />
            <div className="card-text">
              <p className="card-tite">{item.title}</p>
              <p>{item.description}</p>
              <a href="/" className="card-link">
                View resource
              </a>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Portfolio;
