import React from "react";
import { faIcon } from "@fortawesome/free-solid-svg-icons"; // Replace 'faIcon' with the actual icon you want to use
import Navigation from "./Navigation";
import Button from "./Button";
import PhotoBox from "./PhotoBox";

const Panel = () => {
  return (
    <div className="panel">
      <header>
        <h1>Main Navigation</h1>
        <Navigation />
      </header>
      <section>
        <PhotoBox className={["", "", ""]} />
      </section>
      <footer>
        <Button icon={faIcon} text="Click Here" />
      </footer>
    </div>
  );
};

export default Panel;
