import React from "react";
import image from "./../assets/images/Ellipse 1.png";
import "./../assets/styles/Feedback.scss";

const Feedback = ({ data }) => {
  return (
    <div>
      <h2 className="feedback" id="feedbacks">
        Feedbacks
      </h2>
      {data.map((item, index) => (
        <div key={index} data-testid="feedback-item">
          <div className="feedback-box" key={index}>
            <p className="feedback-text">{item.feedback}</p>

            <hr />
          </div>
          <div className="feedback-person">
            <img src={image} />
            <p className="feedback-reporter">
              <a href={item.reporter.citeUrl}>{item.reporter.name} Reporter</a>
            </p>
          </div>
        </div>
      ))}
    </div>
  );
};
export default Feedback;
