import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";
import Box from "./../components/Box";
import PhotoBox from "./../components/PhotoBox";
import TimeLine from "./../components/TimeLine";
import Experience from "./../components/Experience";
import Portfolio from "./../components/Portfolio";
import Address from "./../components/Address";
import Feedback from "./../components/Feedback";
import Navigation from "./../components/Navigation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBars,
  faChevronLeft,
  faChevronUp,
} from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import Skills from "../components/Skills";
import { useDispatch, useSelector } from "react-redux";
import { getSkillsData } from "../store/skills-action";
import { getEducationsData } from "../store/educations-action";

const Inner = () => {
  const [isNavigationVisible, setNavigationVisible] = useState(true);
  const dispatch = useDispatch();
  const educations = useSelector((state) => state.educations.educations);
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    dispatch(getSkillsData());
    dispatch(getEducationsData());
  }, []);

  const hideNavigation = () => {
    setNavigationVisible(!isNavigationVisible);
  };

  return (
    <div className="inner">
      <div
        className={
          "inner-navigation-holder " +
          (isNavigationVisible
            ? "inner-navigation-holder-active"
            : "inner-navigation-holder-inactive")
        }
      ></div>
      <div
        className={
          "inner-navigation " +
          (isNavigationVisible
            ? "inner-navigation-active"
            : "inner-navigation-inactive")
        }
      >
        <div className="navigation-btn" onClick={hideNavigation}>
          <FontAwesomeIcon icon={faBars} />
        </div>
        {isNavigationVisible && (
          <>
            <PhotoBox
              name="John Doe"
              avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
              className={["inner-photobox", "inner-img"]}
            />
            <Navigation />
            <NavLink className="inner-btn" to={"/"}>
              <FontAwesomeIcon icon={faChevronLeft} />
              <span className="inner-btn-text">Go back</span>
            </NavLink>
          </>
        )}
      </div>
      <div className="inner-about">
        <Box
          title="About Me"
          content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque"
        ></Box>
        <TimeLine data={educations} />
        <Experience
          data={[
            {
              date: "2013-2014",
              info: {
                company: "Google",
                job: "Front-end developer / php programmer",
                description:
                  "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
              },
            },
            {
              date: "2012",
              info: {
                company: "Twitter",
                job: "Web developer",
                description:
                  "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
              },
            },
          ]}
        />
        <Skills />
        <Portfolio />
        <Address
          phone="500 342 242"
          email="office@kamsolutions.pl"
          facebook="https://www.facebook.com/facebook"
          twitter="https://twitter.com/wordpress"
          skype="kamsolutions.pl"
        />
        <Feedback
          data={[
            {
              feedback:
                " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
              reporter: {
                photoUrl: "./user.jpg",
                name: "John Doe",
                citeUrl: "https://www.citeexample.com",
              },
            },
            {
              feedback:
                " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolorLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor Aenean commodo ligula eget dolor",
              reporter: {
                photoUrl: "./user.jpg",
                name: "John Doe",
                citeUrl: "https://www.citeexample.com",
              },
            },
          ]}
        />
        <div className="up-btn" onClick={scrollToTop}>
          <FontAwesomeIcon size="1x" icon={faChevronUp} color="white" />
        </div>
      </div>
    </div>
  );
};

export default Inner;
