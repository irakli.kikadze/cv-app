import React from "react";
import { NavLink } from "react-router-dom";
import PhotoBox from "./../components/PhotoBox";

const Home = () => {
  return (
    <div className="home-page">
      <PhotoBox
        name="John Doe"
        title="Programmer. Creative. Innovator"
        description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
        avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
        className={["home-photobox", "photobox-img", "photobox-name"]}
      />
      <NavLink className="home-button" to={"/inner"}>
        Know more
      </NavLink>
    </div>
  );
};

export default Home;
