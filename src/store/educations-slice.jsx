import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  educations: [],
};

const educationsSlice = createSlice({
  name: "educations",
  initialState,
  reducers: {
    updateEduations(state, action) {
      state.educations = action.payload.educations;
    },
  },
});

export const educationsActions = educationsSlice.actions;

export default educationsSlice;
