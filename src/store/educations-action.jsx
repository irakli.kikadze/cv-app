import { educationsActions } from "./educations-slice";
const API_BASE_URL = "http://localhost:3000";

export const getEducationsData = () => {
  return async (dispatch) => {
    try {
      const response = await fetch(`${API_BASE_URL}/api/educations`);
      if (!response.ok) {
        throw new Error("Failed to fetch community data");
      }
      const educationsData = await response.json();
      dispatch(
        educationsActions.updateEduations({ educations: educationsData })
      );
    } catch (error) {
      dispatch(educationsActions.updateEduations({ educations: null }));
      console.error("An error occurred:", error);
    }
  };
};
