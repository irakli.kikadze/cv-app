import { configureStore } from "@reduxjs/toolkit";

import skillsSlice from "./skills-slice.jsx";
import educationsSlice from "./educations-slice.jsx";

const store = configureStore({
  reducer: { skills: skillsSlice.reducer, educations: educationsSlice.reducer },
});

export default store;
