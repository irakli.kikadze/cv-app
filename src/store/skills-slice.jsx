import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  skills: [],
};

const skillsSlice = createSlice({
  name: "skills",
  initialState,
  reducers: {
    updateSkills(state, action) {
      state.skills = action.payload.skills;
    },
    addSkill(state, action) {
      state.skills = [...state.skills, action.payload.skill];
    },
  },
});

export const skillsActions = skillsSlice.actions;

export default skillsSlice;
