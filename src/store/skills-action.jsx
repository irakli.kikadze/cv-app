import { skillsActions } from "./skills-slice";
const API_BASE_URL = "http://localhost:3000";

export const getSkillsData = () => {
  return async (dispatch) => {
    try {
      const response = await fetch(`${API_BASE_URL}/api/skills`);
      if (!response.ok) {
        throw new Error("Failed to fetch community data");
      }
      const skillsData = await response.json();

      dispatch(skillsActions.updateSkills({ skills: skillsData }));
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };
};
export const addSkill = ({ skillName, skillRange }) => {
  const skill = { name: skillName, range: skillRange };
  return async (dispatch) => {
    try {
      const response = await fetch(`${API_BASE_URL}/api/skills`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(skill),
      });

      if (!response.ok) {
        throw new Error("Failed to add skill");
      }

      dispatch(skillsActions.addSkill({ skill }));
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };
};
