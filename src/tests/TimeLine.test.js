import { render, screen } from "@testing-library/react";
import TimeLine from "./../components/TimeLine";
import "@testing-library/jest-dom";
import React from "react";

describe("TimeLine component", () => {
  const data = [
    { date: "2022-01-01", title: "Title 1", text: "Text 1" },
    { date: "2022-01-02", title: "Title 2", text: "Text 2" },
  ];

  it("renders the component with data", () => {
    render(<TimeLine data={data} />);
    const educationElement = screen.getByText("Education");
    expect(educationElement).toBeInTheDocument();
    data.forEach((event) => {
      const dateElement = screen.getByText(event.date);
      const titleElement = screen.getByText(event.title);
      const textElement = screen.getByText(event.text);
      expect(dateElement).toBeInTheDocument();
      expect(titleElement).toBeInTheDocument();
      expect(textElement).toBeInTheDocument();
    });
  });

  it("renders the component with no data", () => {
    render(<TimeLine data={null} />);
    const educationElement = screen.getByText("Education");
    expect(educationElement).toBeInTheDocument();
    const errorMessage = screen.getByText(
      "Something went wrong; please review your server connection!"
    );
    expect(errorMessage).toBeInTheDocument();
  });

  it("renders the component with empty data", () => {
    render(<TimeLine data={[]} />);
    const educationElement = screen.getByText("Education");
    expect(educationElement).toBeInTheDocument();
    const syncIconElement = screen.getByTestId("sync-icon");
    expect(syncIconElement).toBeInTheDocument();
  });
});
