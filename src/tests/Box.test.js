import React from "react";
import Box from "./../components/Box";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

jest.mock("./../assets/styles/Box.scss", () => ({})); // Mock the SCSS import

test("renders box component with provided props", () => {
  const title = "Test Title";
  const content = "Test Content";
  const { getByText } = render(<Box title={title} content={content} />);

  expect(screen.getByText(title)).toBeInTheDocument();
  expect(screen.getByText(content)).toBeInTheDocument();
});
