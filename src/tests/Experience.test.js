import React from "react";
import Experience from "../components/Experience";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

describe("Experience", () => {
  const data = [
    {
      info: {
        company: "Example Company",
        job: "Example Job",
        description: "Example Description",
      },
      date: "Example Date",
    },
  ];

  test("renders the Experience component with provided data", () => {
    const { getByText } = render(<Experience data={data} />);

    const companyText = "Example Company";
    const jobText = /Example Job/i;
    const descriptionText = /Example Description/i;

    expect(screen.getByText(companyText)).toBeInTheDocument();
    expect(screen.getByText(jobText)).toBeInTheDocument();
    expect(screen.getByText(descriptionText)).toBeInTheDocument();
  });
});
