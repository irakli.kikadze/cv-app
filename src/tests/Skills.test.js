import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import Skills from "./../components/Skills";
import "@testing-library/jest-dom";
import React from "react";

const mockStore = configureMockStore();
const store = mockStore({
  skills: {
    skills: [
      { name: "Skill 1", range: 30 },
      { name: "Skill 2", range: 60 },
    ],
  },
});

describe("Skills component", () => {
  it("renders the Skills component with the correct title", () => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>
    );

    const skillsTitle = screen.getByText(/Skills/i);
    expect(skillsTitle).toBeInTheDocument();
  });

  it("toggles the SkillForm component when the edit button is clicked", () => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>
    );

    const editButton = screen.getByText(/Open Edit/i);
    fireEvent.click(editButton);

    const skillForm = screen.getByTestId("skill-form");
    expect(skillForm).toBeInTheDocument();
  });

  // Add more test cases as needed
});
