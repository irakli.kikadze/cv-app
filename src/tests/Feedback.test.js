import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Feedback from "./../components/Feedback";
jest.mock("./../assets/images/Ellipse 1.png", () => "test-file-stub");

describe("Feedback Component", () => {
  const mockData = [
    {
      feedback: "This is a great product!",
      reporter: { name: "John Doe", citeUrl: "#" },
    },
    {
      feedback: "Amazing service. Highly recommended.",
      reporter: { name: "Jane Doe", citeUrl: "#" },
    },
  ];

  test("renders feedback component with provided data", () => {
    render(<Feedback data={mockData} />);

    const feedbackTitle = screen.getByText("Feedbacks");

    expect(feedbackTitle).toBeInTheDocument();
  });

  test("renders correct number of feedback items", () => {
    render(<Feedback data={mockData} />);

    const feedbackItems = screen.getAllByTestId(/feedback-item/i);
    expect(feedbackItems.length).toBe(mockData.length);
  });
});
