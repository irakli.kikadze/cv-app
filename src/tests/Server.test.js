import { makeServer } from "./../services/server";
import { Server } from "miragejs";

describe("makeServer function", () => {
  let server;

  beforeEach(() => {
    server = makeServer({ environment: "test" });
  });

  afterEach(() => {
    server.shutdown();
  });

  test("it should return an education object", () => {
    const education = server.create("education", {
      date: 2001,
      title: "Title 0",
      text: "Sample text",
    });

    expect(education.date).toEqual(2001);
    expect(education.title).toEqual("Title 0");
    expect(education.text).toEqual("Sample text");
  });

  test("it should return a skill object", async () => {
    const skill = await server.post("/skills", {
      name: "New Skill",
      range: 50,
    });
  });
});
