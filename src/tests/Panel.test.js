import { render, screen } from "@testing-library/react";
import Panel from "../components/Panel";
import React from "react";
import "@testing-library/jest-dom";

describe("Panel Component", () => {
  it("renders the Button component in the footer", () => {
    render(<Panel />);
    const buttonElement = screen.getByRole("button", { name: "Click Here" });
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement).toHaveTextContent("Click Here");
  });
});
