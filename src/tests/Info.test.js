import React from "react";
import { render, screen } from "@testing-library/react";
import Info from "./../components/Info";
import "@testing-library/jest-dom";

describe("Info Component", () => {
  test("renders text correctly", () => {
    const testText = "Test text";
    render(<Info text={testText} />);
    const textElement = screen.getByText(testText);
    expect(textElement).toBeInTheDocument();
  });
});
