import React from "react";
import { render, screen } from "@testing-library/react";
import Button from "./../components/Button";
import { faCoffee } from "@fortawesome/free-solid-svg-icons"; // Import the faCoffee icon here

describe("Button", () => {
  test("renders button with text and icon if provided", () => {
    render(<Button icon={faCoffee} text="Click Me" />); // Use faCoffee icon
    const buttonText = screen.getByText("Click Me");
    const buttonIcon = screen.getByTestId("button-icon");
  });

  test("renders button with text only if no icon provided", () => {
    render(<Button text="Click Me" />);
    const buttonText = screen.getByText("Click Me");
    const buttonIcon = screen.queryByTestId("button-icon");
  });
});
